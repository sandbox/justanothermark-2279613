<?php
/**
 * @file
 * BehatGenerateSelectProcessor class for default behaviour.
 */

/**
 * Class BehatGenerateSelectProcessor.
 */
class BehatGenerateSelectProcessor implements BehatGenerateWidgetProcessorInterface {
  /**
   * {@inheritdoc}
   */
  public function generate($field_instance) {
    $output = '';

    $widget_type = $field_instance['widget']['type'];
    $field_base = field_info_field($field_instance['field_name']);
    $options = $field_base['settings']['allowed_values'];

    switch ($widget_type) {
      case 'options_select':
        // Select a random option.
        $output .= "    And I select \"" . $this->randomOption($options) . "\" from \"" . $field_instance['label'] . "\"\n";
        break;

      case 'options_buttons':
        if ($field_base['cardinality'] == 1) {
          // Check a random radio button.
          $output .= "    And I select the radio button \"" . $this->randomOption($options) . "\"\n";
        }
        else {
          $cardinality = $field_base['cardinality'];
          $number_to_select = rand(1, $cardinality);

          $selected_options = array();
          for ($i = 0; $i < $number_to_select; $i++) {
            // Check random checkboxes for each option.
            $random_option = $this->randomOption($options, $selected_options);
            $selected_options[] = $random_option;
            $output .= "    And I check the box \"" . $random_option . "\"\n";
          }
        }
        break;
    }

    return $output;
  }
}
