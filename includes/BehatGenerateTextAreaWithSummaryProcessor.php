<?php
/**
 * @file
 * Class BehatGenerateTextAreaWithSummaryProcessor.
 */

/**
 * Class BehatGenerateTextAreaWithSummaryProcessor.
 */
class BehatGenerateTextAreaWithSummaryProcessor implements BehatGenerateWidgetProcessorInterface {

  /**
   * {@inheritdoc}
   */
  public function generate($field_instance) {
    $output = '';

    $widget_type = $field_instance['widget']['type'];
    switch ($widget_type) {
      case 'text_textarea_with_summary':
        // Fill in the summary option.
        $output .= "    And I fill in \"" . BehatGenerateTextfieldProcessor::randomString() . "\" for \"" . $field_instance['field_name'] . "[und][0][summary]\"\n";
        // Fill in the body option.
        $output .= "    And I fill in \"" . BehatGenerateTextfieldProcessor::randomString() . "\" for \"" . $field_instance['label'] . "\"\n";
        // Select a valid text format.
        if (!empty($field_instance['settings']['better_formats']['allowed_formats'])) {
          $options = array_filter($field_instance['settings']['better_formats']['allowed_formats']);
        }
        else {
          $options = array_keys(filter_formats());
        }
        $output .= "    And I select \"" . BehatGenerateTextfieldProcessor::randomOption($options) . "\" from \"" . $field_instance['field_name'] . "[und][0][format]\"\n";

        break;

      case 'text_textarea':
        // Fill in the body option.
        $output .= "    And I fill in \"" . BehatGenerateTextfieldProcessor::randomString() . "\" for \"" . $field_instance['label'] . "\"\n";
        // Select a valid text format.
        if (!empty($field_instance['settings']['better_formats']['allowed_formats'])) {
          $options = array_filter($field_instance['settings']['better_formats']['allowed_formats']);
        }
        else {
          $options = array_keys(filter_formats());
        }
        $output .= "    And I select \"" . BehatGenerateTextfieldProcessor::randomOption($options) . "\" from \"" . $field_instance['field_name'] . "[und][0][format]\"\n";
        break;
    }

    return $output;
  }
}
