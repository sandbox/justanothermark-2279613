<?php
/**
 * @file
 * BehatGenerateWidgetProcessorInterface interface.
 */

/**
 * Interface BehatGenerateWidgetProcessorInterface
 */
interface BehatGenerateWidgetProcessorInterface {
  /**
   * Generates the output for a field instance.
   *
   * @param array $field_instance
   *   The field instance to generate a Behat test for.
   */
  public function generate($field_instance);
}
