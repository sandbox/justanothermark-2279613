<?php
/**
 * @file
 * BehatGenerateTextfieldProcessor class for default behaviour.
 */

/**
 * Class BehatGenerateTextfieldProcessor
 */
class BehatGenerateTextfieldProcessor implements BehatGenerateWidgetProcessorInterface {

  /**
   * {@inheritdoc}
   */
  public function generate($field_instance) {
    $output = '';

    $output .= "    And I fill in \"" . $this->randomString(50) . "\" for \"" . $field_instance['label'] . "\"\n";

    return $output;
  }

  /**
   * Random string generator.
   *
   * @param int $length
   *   The length of the random string to be generated.
   *
   * @return string
   *   The generated random string.
   */
  public static function randomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ ';
    $random_string = '';
    for ($i = 0; $i < $length; $i++) {
      $random_string .= $characters[rand(0, strlen($characters) - 1)];
    }
    return $random_string;
  }

  /**
   * Select a random option from a random array of options.
   *
   * @param array $options
   *   The array of options to select from.
   * @param array $selected
   *   An optional array of already selected options.
   *
   * @return mixed
   *   The value of a random option.
   */
  public static function randomOption($options, $selected = array()) {
    $options = array_values(array_diff($options, $selected));
    return $options[rand(0, count($options) - 1)];
  }
}
