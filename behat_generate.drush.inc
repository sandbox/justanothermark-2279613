<?php

/**
 * @file
 * Drush command for generating Behat tests for node add forms.
 */

/**
 * Implements hook_drush_command().
 */
function behat_generate_drush_command() {
  $items = array();

  $items['behat-generate-node'] = array(
    'description' => dt('Generate Behat tests for node add forms'),
    'aliases' => array('bgn'),
    'arguments' => array(
      'types' => dt('A space separated list of node types.'),
    ),
    'examples' => array(
      'drush behat-node-generate' => 'Generate Behat tests for all content types',
      'drush behat-node-generate page' => 'Generate Behat tests for the Basic Page content type',
    ),
  );

  return $items;
}

/**
 * Drush callback for generating Behat tests for node types.
 */
function drush_behat_generate_node() {
  $args = func_get_args();
  $types = empty($args) ? array_keys(node_type_get_types()) : $args;

  // Process each node type.
  foreach ($types as $type) {
    $output = '';
    $output = <<<EOF
@api
Feature: $type
  In order to display $type content.
  As an admin
  I need to be able to create $type content

  Scenario Outline: Admins can add $type content

  Given I am logged in as a user with the "<role>" role
    And I go to "node/add/$type"

EOF;
    // Add a step for title property.
    $title = BehatGenerateTextfieldProcessor::randomString();
    $output .= "    And I fill in \"$title\" for \"title\"\n";

    // Load all fields on the current content type.
    $field_instances = field_info_instances('node', $type);
    // Add a step for each Field API field.
    foreach ($field_instances as $field_instance) {
      $widget_type = $field_instance['widget']['type'];

      // Find the relevant processor or use the default.
      $processor = behat_generate_get_widget_processor($widget_type);

      $output .= $processor->generate($field_instance);
    }

    $output .= <<<EOF
  When I press "Save"
    Then I should see the message "$title has been created."

  Examples:
    | role                  |
    | administrator         |

EOF;

    drush_print($output);
  }
}
